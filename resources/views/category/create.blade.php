@extends('layouts.admin')

@section('content')



    <div class="panel">

        <div class="header">افزودن دسته جدید</div>


        <div class="panel_content">


            {!! Form::open(['url' => 'admin/category','files'=>true]) !!}

            <div class="form-group">

                {{ Form::label('name','نام دسته : ') }}
                {{ Form::text('name',null,['class'=>'form-control']) }}
                @if($errors->has('name'))
                    <span class="has_error">{{ $errors->first('name') }}</span>
                @endif
            </div>

            <div class="form-group">

                {{ Form::label('ename','نام انگلیسی دسته : ') }}
                {{ Form::text('ename',null,['class'=>'form-control']) }}

            </div>

            <div class="form-group">

                {{ Form::label('search_url','url دسته : ') }}
                {{ Form::text('search_url',null,['class'=>'form-control']) }}
                @if($errors->has('search_url'))
                    <span class="has_error">{{ $errors->first('search_url') }}</span>
                @endif
            </div>



            <div class="form-group">
                {{ Form::label('notShow','عدم نمایش در لیست اصلی : ') }}
                {{ Form::checkbox('notShow',false) }}
            </div>

            <button class="btn btn-success">ثبت دسته</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
